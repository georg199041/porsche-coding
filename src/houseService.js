import c from './config';

export default {
	get: async (id) => (
		id ? fetch(`${c.baseUrl}/houses/${id}`).then(res => res.json())
		: fetch(`${c.baseUrl}/houses`).then(res => res.json())
	),
	getByUrls: async (urls) => (
		await Promise.all(urls.map(url => fetch(url).then(res => res.json())))
	)
};