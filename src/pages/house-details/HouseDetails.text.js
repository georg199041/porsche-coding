import React from 'react';
import HouseDetails from './HouseDetails';
import renderer from 'react-test-renderer';

it('HouseDetails renders correctly', () => {
  const tree = renderer
    .create(<HouseDetails />)
    .toJSON();
  expect(tree).toMatchSnapshot();
});