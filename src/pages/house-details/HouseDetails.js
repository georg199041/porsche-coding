import React from 'react';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';

import houseService from '../../houseService';

export default class HouseDetails extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			house: {},
			members: []
		};
	}
	async componentDidMount() {
		const house = await houseService.get(this.props.match.params.id);
		const members = house.swornMembers.length ? await houseService.getByUrls(house.swornMembers) : [];
		this.setState((state) => ({
			house,
			members
		}));
	}
	render() {
		return (
			<Card>
				<CardContent>
					<strong>Details of the: {this.state.house.name}</strong>
					{this.state.members.length ? <div>Members of the house:
						<ul>{this.state.members.map(member => (
							<li key={member.name}>
								{member.titles.map(title => <span key={title}>{title} </span>)}
								 <span>{member.name}</span>, Gender - {member.gender}
							</li>
						))}</ul>
					</div> : <div>The house has no members</div>}
				</CardContent>
			</Card>
		);
	}    
};
