import React from 'react';
import HouseList from './HouseList';
import renderer from 'react-test-renderer';

it('HouseList renders correctly', () => {
  const tree = renderer
    .create(<HouseList />)
    .toJSON();
  expect(tree).toMatchSnapshot();
});