import React from 'react';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import { Link } from 'react-router-dom'

import houseService from '../../houseService';

export default class HouseList extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			houses: []
		};
	}
	async componentDidMount() {
		const houses = await houseService.get();
		this.setState((state) => ({
			houses 
		}));
	}
	render() {
		return (
			<Table>
		        <TableHead>
		          <TableRow>
		            <TableCell>House name</TableCell>
		            <TableCell numeric>Region</TableCell>
		            <TableCell numeric>Coat of arms</TableCell>
		          </TableRow>
		        </TableHead>
		        <TableBody>
		          {this.state.houses.map(row => {
	          			const url = row.url.split('/'),
	          			id = url[url.length -1];

		              return (
		              	<TableRow key={id}>
			                <TableCell component="th" scope="row">
			                  <Link to={`/house/${id}`}>{row.name}</Link>
			                </TableCell>
			                <TableCell numeric>{row.region}</TableCell>
			                <TableCell numeric>{row.coatOfArms}</TableCell>
			            </TableRow>
		            )})}
		        </TableBody>
		    </Table>
		);
	}
};