import React, { Component } from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import './App.css';

import HouseList from './pages/house-list/HouseList';
import HouseDetails from './pages/house-details/HouseDetails';

class App extends Component {
  render() {
    return (
      <Router>
        <div className="App">
          <header className="App-header">
            <h1 className="App-title">Game of Thrones houses</h1>
          </header>
          <div>
            <Route exact path="/" component={HouseList} />
            <Route exact path="/house/:id" component={HouseDetails} />
          </div>
        </div>
      </Router>
    );
  }
}

export default App;
